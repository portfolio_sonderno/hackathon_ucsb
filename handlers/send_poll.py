from aiogram import types
from poll_actions import send_pool_to_user

async def send_poll(msg): 
    await send_pool_to_user(msg.chat.id, msg.chat.username, msg.chat.first_name, msg.chat.last_name)
