import os
import json
from aiogram import types
from dotenv import load_dotenv

# connect environment variables
dotenv_path = os.path.join(os.path.dirname(__file__), '../', '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

async def send_welcome(msg: types.Message):
    await msg.answer("👋 Приветствую. \nЭтот бот создан для ответа на опросы. \nНажмите /run для начала опроса.")
    if str(msg.chat.id) in json.loads(os.environ.get('ADMIN_CHAT_ID')):
        await msg.answer("У вас также есть права администратора. \nВы можете создать новый опрос по команде /admin")
        
