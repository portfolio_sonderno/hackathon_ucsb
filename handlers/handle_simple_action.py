from aiogram import types
from save_to_csv import create_csv_with_answers

async def handle_simple_action(msg: types.Message):
    if msg.text == 'Закрыть':
        remove_keyboard = types.ReplyKeyboardRemove()
        await msg.answer("Меню закрыто", reply_markup = remove_keyboard)
        return

    if msg.text == 'Просмотр ответов':
        await msg.answer("Создается csv-файл")
        create_csv_with_answers()
        await msg.answer_document(types.InputFile('result.csv'))
        return
    
    if msg.text[0] != '/':
        await msg.answer("В этом боте можно лишь отвечать на приходящие опросы.")

        

