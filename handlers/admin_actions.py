import os
import json
from aiogram import types
from tg_buttons import add_poll_button, search_button, cancel_button
from dotenv import load_dotenv

# connect environment variables
dotenv_path = os.path.join(os.path.dirname(__file__), '../', '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)


async def admin_actions(msg: types.Message):
    if str(msg.chat.id) not in json.loads(os.environ.get('ADMIN_CHAT_ID')):
        await msg.answer("У вас нет прав администратора, вы можете только отвечать на опросы.")
        return

    poll_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    poll_keyboard.add(add_poll_button, search_button, cancel_button)
    await msg.answer("Вы можете создать новый опрос и посмотреть ответы всех пользователей", reply_markup=poll_keyboard)

