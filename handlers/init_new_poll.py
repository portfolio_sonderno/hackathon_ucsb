import json
from aiogram import types
from poll_actions import create_new_poll

async def init_new_poll(msg: types.Message):
    question = msg.poll.question
    options = msg.poll.options

    await create_new_poll(question, options)