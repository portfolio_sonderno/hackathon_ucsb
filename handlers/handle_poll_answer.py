from aiogram import types
from poll_actions import save_user_answer, send_pool_to_user

async def handle_poll_answer(msg: types.PollAnswer):
    save_user_answer(msg.user.id, msg.option_ids[0] + 1)
    await send_pool_to_user(msg.user.id, msg.user.username, msg.user.first_name, msg.user.last_name)