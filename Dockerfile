FROM python:3.8.3-slim-buster
ENV PYTHONUNBUFFERED 1
RUN apt-get update \
    && apt-get -y install libpq-dev gcc \
    && pip install psycopg2
COPY requirements.txt ./requirements.txt
COPY . ./
RUN pip install -r requirements.txt
CMD ["python","main.py"]
