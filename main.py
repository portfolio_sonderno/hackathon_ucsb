import os
from aiogram import executor, types
from init_bot import bot, dp
from handlers import *

# Handlers
dp.register_message_handler(send_welcome, commands=['start'])

dp.register_message_handler(send_poll, commands=['run'])

dp.register_message_handler(admin_actions, commands=["admin"])

# Прослушивание события создания опроса админом
dp.register_message_handler(init_new_poll, content_types=["poll"])

# Прослушивание события ответа на опрос
dp.register_poll_answer_handler(handle_poll_answer)

dp.register_message_handler(handle_simple_action)



if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)