import csv
from data_base import db

def create_csv_with_answers():
    with open('result.csv','w+',encoding='cp1251',newline='') as f:
        writer = csv.writer(f, delimiter=',')
        for row in db.show_all_answers():
            writer.writerow(row)
