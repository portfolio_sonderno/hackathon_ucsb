from data_base.postgre_db import Postgre_db 

class Ucsb_db(Postgre_db):
	__query_create_questions_table = """
	CREATE TABLE IF NOT EXISTS questions (
	  id SERIAL PRIMARY KEY,
	  question_content TEXT,
	  question_options TEXT[]
	)
	"""

	__query_create_users_table = """
	CREATE TABLE IF NOT EXISTS users (
	  id SERIAL PRIMARY KEY,
	  tg_chat_id BIGINT NOT NULL UNIQUE, 
	  tg_username TEXT,
	  tg_first_name TEXT,
	  tg_last_name TEXT
	  	)
	"""

	__query_create_poll_notes_table = """
	CREATE TABLE IF NOT EXISTS poll_notes (
	  id SERIAL PRIMARY KEY,
	  user_id INTEGER REFERENCES users(id),
	  question_id INTEGER REFERENCES questions(id),
	  answer_num INTEGER
	);
	"""

	__query_all_answers = """
	select users.tg_first_name as Имя, users.tg_last_name as Фамилия, users.tg_username as Никнейм, questions.question_content as Вопрос, questions.question_options[answer_num] as Ответ 
	from questions
	join poll_notes on poll_notes.question_id = questions.id
	join users on users.id = poll_notes.user_id;
	"""

	def create_qestions_table(self):
		self.execute_query(Ucsb_db.__query_create_questions_table)

	def create_poll_notes_table(self):
		self.execute_query(Ucsb_db.__query_create_poll_notes_table)

	def create_users_table(self):
		self.execute_query(Ucsb_db.__query_create_users_table)

	def show_all_answers(self):
		try:
			all_answers = self.execute_read_query("""
					select users.tg_first_name as Имя, users.tg_last_name as Фамилия, users.tg_username as Никнейм, questions.question_content as Вопрос, questions.question_options[answer_num] as Ответ 
					from questions
					join poll_notes on poll_notes.question_id = questions.id
					join users on users.id = poll_notes.user_id;
					""")

			if all_answers:
				return all_answers
		except OSError as e:
			print('Error on show_all_answers', e)

	def get_all_ready_users(self):
		try:
			users_ids =  self.execute_read_query(f"""
				select tg_chat_id
				from poll_notes 
				left join users on poll_notes.user_id = users.id  
				where question_id = (select id from questions order by id desc limit 1)
				order by poll_notes.question_id desc
				""")

			return list(map(lambda user_id: user_id[0], users_ids))

		except OSError as e:
			print('Error on get_all_ready_users: ', e)


	def add_new_question(self, question_content, question_options):
		question_options = "{" + question_options + "}"

		try:
			self.execute_query(f"""INSERT INTO questions (question_content, question_options) 
							VALUES ('{question_content}', '{question_options}')""")

			print(f'New poll was added to data base successfuly')

		except OSError as e:
			print('Error on add_new_question: ', e)



	def create_user_and_read_id(self, tg_chat_id, tg_username, tg_first_name, tg_last_name):
		try:
			return self.execute_read_query(f"""
				INSERT INTO users (tg_chat_id, tg_username, tg_first_name, tg_last_name)
				SELECT * FROM (SELECT {tg_chat_id}, '{tg_username}', '{tg_first_name}', '{tg_last_name}') AS tmp
				WHERE NOT EXISTS (
				    SELECT id FROM users WHERE tg_chat_id = {tg_chat_id}
				) LIMIT 1 ;
				select id
				from users
				where tg_chat_id = {tg_chat_id}""")[0][0]

		except OSError as e:
			print('Error on read_user_id: ', e)


	def read_user_id(self, tg_chat_id):		
		try:
			return self.execute_read_query(f"""
				select id
				from users
				where tg_chat_id = {tg_chat_id}""")[0][0]

		except OSError as e:
			print('Error on read_user_id: ', e)


	def append_poll_note(self, user_id, question_id, answer_num):
		try:
			self.execute_query(f"""
				insert into poll_notes (user_id, question_id, answer_num) 
				SELECT * FROM (SELECT {user_id}, {question_id}, {answer_num}) AS tmp
				WHERE NOT EXISTS (
				    SELECT id FROM poll_notes WHERE user_id = {user_id} and question_id = {question_id}
				) LIMIT 1;
				""")

			print(f'Answer was added to user successfuly')

		except OSError as e:
			print('Error on append_poll_note: ', e)


	def read_last_user_poll_note(self, user_id):
		try:
			poll_id = self.execute_read_query(f"""
						select question_id
						from poll_notes 
						left join users on poll_notes.user_id = users.id 
						where users.id = {user_id}
						order by poll_notes.question_id desc
						limit 1
						""")
			return poll_id[0][0] if poll_id else 0

		except OSError as e:
			print('Error on read_last_user_poll_note: ', e)


	def read_next_question(self, question_id):
		try:
			question_data = self.execute_read_query(f"""
					select id, question_content, question_options 
					from questions where id > {question_id} limit 1""")
			if question_data:
				return question_data[0]

		except OSError as e:
			print('Error on read_next_question: ', e)

