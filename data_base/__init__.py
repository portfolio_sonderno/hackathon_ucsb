import os
from dotenv import load_dotenv
from data_base.ucsb_db import Ucsb_db

# connect environment variables
dotenv_path = os.path.join(os.path.dirname(__file__), '../', '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

db_creds = { 'name': os.environ.get('DB_NAME'),
            'user': os.environ.get('DB_USER'),
            'password': os.environ.get('DB_PASSWORD'),
            'host': os.environ.get('DB_HOST'),
            'port': os.environ.get('DB_PORT')}

db = Ucsb_db(db_creds['name'], db_creds['user'], db_creds['password'], db_creds['host'], db_creds['port'])

db.create_qestions_table()
db.create_users_table()
db.create_poll_notes_table()
