from init_bot import bot, dp
from data_base import db

async def send_pool_to_user(chat_id, username, first_name, last_name):
    # Создаем пользователя, если его нет, и возвращаем его id
    user_id = db.create_user_and_read_id(chat_id, username, first_name, last_name)

    # Смотрим на какой последний вопрос он отвечал
    question_id = db.read_last_user_poll_note(user_id)

    # Получаем данные следующего в БД вопроса
    new_question = db.read_next_question(question_id)

    if not new_question: 
            await bot.send_message(chat_id, 'все вопросы кончились, спасибо за ответы')
            return

    quest_id, question, options = new_question

    # Отправляем следующий
    await bot.send_poll(chat_id, question, options, is_anonymous = False, allows_multiple_answers = False)
