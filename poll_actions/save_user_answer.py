from data_base import db

def save_user_answer(tg_chat_id, answer_num):
    # Получаем id пользователя
    user_id = db.read_user_id(tg_chat_id)

    # Смотрим на какой последний вопрос он отвечал
    last_question_id = db.read_last_user_poll_note(user_id)
    
    current_question = db.read_next_question(last_question_id)

    # Добавляем ответ в таблицу poll_notes и получаем его id
    answer_id = db.append_poll_note(user_id, current_question[0], answer_num)


