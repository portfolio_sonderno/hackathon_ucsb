import os
import json
from data_base import db
from init_bot import bot, dp
from dotenv import load_dotenv


# connect environment variables
dotenv_path = os.path.join(os.path.dirname(__file__), '../', '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

async def create_new_poll(question, options):
	old_users =  db.get_all_ready_users()

	str_options = ', '.join(list(map(lambda option: f'"{option.text}"', options)))
	json_arr_options = json.dumps(list(map(lambda option: option.text, options)))

	db.add_new_question(question, str_options)

	for chat_id in old_users:
		if str(chat_id) in json.loads(os.environ.get('ADMIN_CHAT_ID')): continue

		await bot.send_message(chat_id, 'Ответьте пожалуйста но новый опрос!')
		await bot.send_poll(chat_id, question, json_arr_options, is_anonymous = False, allows_multiple_answers = False)

